import lozad from 'lozad'
import isMobile from './is_mobile'

const observer = lozad('.lozad', {
  load: function(el) {
      if(isMobile) {
        el.src = MEDIA_URL + el.getAttribute('data-mobile-src');
      } else {
        el.src = MEDIA_URL + el.getAttribute('data-src');
      }

  }
}); // lazy loads elements with default selector as '.lozad'

observer.observe();