let elements = [
  {
    class:'.section_2_title',
    animation: 'fadeInTop'
  },
  {
    class:'.landing_content_header',
    animation: 'fadeInRight'
  },
  {
    class:'.landing_content_body',
    animation: 'fadeInRight'
  },
  {
    class:'.landing_content_cta',
    animation: 'fadeInRight'
  },
  {
    class:'.step_1',
    animation: 'fadeInTop'
  },
  {
    class:'.step_2',
    animation: 'fadeInTopShort'
  },
  {
    class:'.step_3',
    animation: 'fadeInTopFar'
  },
  {
    class:'.register_cta_button',
    animation: 'fadeInTop'
  },
  {
    class:'.p_1',
    animation: 'fadeInTop'
  },
  {
    class:'.p_2',
    animation: 'fadeInTop'
  },
  {
    class:'.p_3',
    animation: 'fadeInTop'
  },
]


function isElementVisible(el) {
   //special bonus for those using jQuery
   if (typeof jQuery !== 'undefined' && el instanceof jQuery) el = el[0];

   var rect = el.getBoundingClientRect();
   // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
   var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
   var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

   // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
   var vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
   var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

   return (vertInView && horInView);
}

function checkElements(elems) {
  elems.forEach(elem => {
    const $elem = $(`${elem.class}.viewport_target`)[0];

    if ($elem && !$elem.classList.contains(elem.animation)) {
      if (isElementVisible($elem)) {
        
        $elem.classList += ` ${elem.animation}`
      }
    }

  })
}
var scrolling = false;
 
$(window).on('load', function() {
  checkElements(elements);
  $( window ).scroll( function() {
    scrolling = true;
  });
   
  setInterval( function() {
    if ( scrolling ) {
      scrolling = false
      checkElements(elements);
      
    }
  }, 250 );
});
