import jquery from "jquery";
window.$ = window.jQuery = jquery;
import './snippets/sticky_header';
import './snippets/is_mobile';
import './snippets/lozad_loader';
import './snippets/is_in_viewport';

if (module.hot) {
  module.hot.accept();
}

$(document).ready(function(){
	$('#nav-icon4').click(function(){
    $(this).toggleClass('open');
    $('.header_mobile_page').toggleClass('header_open')
    $('.header_mobile_page_link').toggleClass('fadeInTop');
	});
});

